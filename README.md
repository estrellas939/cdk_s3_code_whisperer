# cdk_s3_code_whisperer

## 1. Install Node.js
- Since CDK is a Node.js applicaiton, we need to download Node.js. Here I use the Node Version Manager (nvm) to [download](https://github.com/nvm-sh/nvm)

## 2. Install AWS Command Line Interface (CLI)
- The AWS Command Line Interface (AWS CLI) is a unified tool to manage AWS services.

## 3. Install AWS CDK
- Install the AWS CDK toolkit globally using npm
```
npm install -g aws-cdk
```

## 4. Create CDK Project
- Initialize a New CDK Project in typescript:
```
cdk init app --language=typescript
```

## 5. Create AWS S3 pkg for CDK
- AWS S3 package for CDK:
```
npm install @aws-cdk/aws-s3
```

## 6.Bootstrap CDK Environment
- This is my first CDK project, so I need to bootstrap the CDK environment to my AWS ID and region
```
cdk bootstrap
```
![bootstrap](https://gitlab.com/estrellas939/cdk_s3_code_whisperer/-/raw/main/pic/bootstrap.png?inline=false)

## 7. Open lib/week3proj-stack.ts in VSCode
- This is the main file of my CDK project.
- Due to we select `app` as the project type, there will be some codes automatically generated in the file.

## 8. Install Amazon Toolkit Extension
- AWS CodeWhisperer is embedded in the Amazon Toolkit extension. Install and log in to start using CodeWhisperer.
![extension](https://gitlab.com/estrellas939/cdk_s3_code_whisperer/-/raw/main/pic/extension.png?inline=false)

## 9. Using CodeWhisperer
- Check status bar to make sure CodeWhisperer is running.
![status bar](https://gitlab.com/estrellas939/cdk_s3_code_whisperer/-/raw/main/pic/Status_Bar.png?inline=false)
- At the beginning of the code, simpily wirte a comment `//Create S3 bucket using AWS CDK with properties like versioning and encryption`
- CodeWhisperer will automatically generate import sentenses and other codes.
- Just tap `tab` and apply the codes.

## 10. Compile ts Code
- After building the code, we need to compile the code to machine-readable language.
```
npm run build
```

## 11. Deploy CDK Stack
- Back to the main directory, deploy the stack to AWS.
```
cdk deploy
```
![deploy](https://gitlab.com/estrellas939/cdk_s3_code_whisperer/-/raw/main/pic/deploy.png?inline=false)

## 12. Check AWS s3 Panel to See the s3 Bucket Meets Requirements
- Go to AWS s3 panel, we can see the new bucket we created using CDK.
![bucket](https://gitlab.com/estrellas939/cdk_s3_code_whisperer/-/raw/main/pic/bucket.png?inline=false)
- Click to see the properties.
![properties](https://gitlab.com/estrellas939/cdk_s3_code_whisperer/-/raw/main/pic/bucketproperties.png?inline=false)

