// build a s3 bucket with versioning and encryption
import * as cdk from 'aws-cdk-lib';
import { aws_s3 as s3 } from 'aws-cdk-lib';

// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class Week3ProjStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here
    new s3.Bucket(this, 'MyFirstBucket', {
      versioned: true, // versioning
      encryption: s3.BucketEncryption.S3_MANAGED, // encryption
    });
    // example resource
    // const queue = new sqs.Queue(this, 'Week3ProjQueue', {
    //   visibilityTimeout: cdk.Duration.seconds(300)
    // });
  }
}
